package com.example.donotusethis.assignment0;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;


/**
 * Created by DO NOT USE THIS! on 1/9/2016.
 */
public class Elements {//this class is to get access to all elements present in the activity

    //All elements needed to be accessed by java classes...
    TextView headTeamName, bodyTeamName, headEntry1, bodyEntry1, headEntry2, bodyEntry2, headEntry3, bodyEntry3;
    TextView headMessage, bodyMessage, headSuggestions, bodySuggestions;
    EditText myInputEntry, myInputName;
    Button myButton;
    RelativeLayout teamName, entry1, entry2, entry3, displayMessage, suggestions;
    InputMethodManager inputMethodManager;

    public Elements(Activity activity) {
        //Text elements present in the layouts
        headTeamName=(TextView)activity.findViewById(R.id.headTeamName);
        bodyTeamName=(TextView)activity.findViewById(R.id.bodyTeamName);
        headEntry1=(TextView)activity.findViewById(R.id.headEntry1);
        bodyEntry1=(TextView)activity.findViewById(R.id.bodyEntry1);
        headEntry2=(TextView)activity.findViewById(R.id.headEntry2);
        bodyEntry2=(TextView)activity.findViewById(R.id.bodyEntry2);
        headEntry3=(TextView)activity.findViewById(R.id.headEntry3);
        bodyEntry3=(TextView)activity.findViewById(R.id.bodyEntry3);
        headMessage=(TextView)activity.findViewById(R.id.headMessage);
        bodyMessage=(TextView)activity.findViewById(R.id.bodyMessage);
        headSuggestions=(TextView)activity.findViewById(R.id.headSuggestions);
        bodySuggestions=(TextView)activity.findViewById(R.id.bodySuggestions);
        //Input elements
        myInputEntry=(EditText)activity.findViewById(R.id.myInputEntry);
        myInputName=(EditText)activity.findViewById(R.id.myInputName);
        //Button element
        myButton=(Button)activity.findViewById(R.id.myButton);
        //Relative Layout
        teamName=(RelativeLayout)activity.findViewById(R.id.teamName);
        entry1=(RelativeLayout)activity.findViewById(R.id.entry1);
        entry2=(RelativeLayout)activity.findViewById(R.id.entry2);
        entry3=(RelativeLayout)activity.findViewById(R.id.entry3);
        displayMessage=(RelativeLayout)activity.findViewById(R.id.displayMessage);
        suggestions=(RelativeLayout)activity.findViewById(R.id.suggestions);
        //input method manager
        inputMethodManager=(InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        //show keyborad and focus
        openKeyboard(myInputName);
    }

    public void openKeyboard(EditText editText){
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
        inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    private void setTextView(TextView textView, String content){
        if (content==null){
            textView.setVisibility(View.INVISIBLE);
        }else{
            textView.setVisibility(View.VISIBLE);
            textView.setText(content);
        }
    }

    public void setMessage(String head, String body){
        setTextView(headMessage, head);
        setTextView(bodyMessage, body);
    }

    public void setEntry(int current_state, String head, String body){
        switch(current_state){
            case 0:setTextView(headTeamName, head);
                setTextView(bodyTeamName, body);
                break;
            case 1:setTextView(headEntry1, head);
                setTextView(bodyEntry1, body);
                break;
            case 2:setTextView(headEntry2, head);
                setTextView(bodyEntry2, body);
                break;
            case 3:setTextView(headEntry3, head);
                setTextView(bodyEntry3, body);
                break;
        }
    }

    public void visibilitySet(boolean boolTeam, boolean boolEntry1,
                              boolean boolEntry2, boolean boolEntry3, boolean boolNumber){
        if (boolTeam){
            teamName.setVisibility(View.VISIBLE);
            teamName.setClickable(true);
        }else{
            teamName.setVisibility(View.INVISIBLE);
            teamName.setClickable(false);
        }
        if (boolEntry1){
            entry1.setVisibility(View.VISIBLE);
            entry1.setClickable(true);
        }else{
            entry1.setVisibility(View.INVISIBLE);
            entry1.setClickable(false);
        }
        if (boolEntry2){
            entry2.setVisibility(View.VISIBLE);
            entry2.setClickable(true);
        }else{
            entry2.setVisibility(View.INVISIBLE);
            entry2.setClickable(false);
        }
        if (boolEntry3){
            entry3.setVisibility(View.VISIBLE);
            entry3.setClickable(true);
        }else{
            entry3.setVisibility(View.INVISIBLE);
            entry3.setClickable(false);
        }
        if (boolNumber){
            myInputEntry.setVisibility(View.VISIBLE);
//            myInputEntry.setEnabled(true);
        }else{
            myInputEntry.setVisibility(View.INVISIBLE);
//            myInputEntry.setEnabled(false);
        }
    }

    public void setStateText(int final_state){
        switch (final_state){
            case 0:myButton.setText("set team name");
                visibilitySet(false, false, false, false, false);
                myInputName.setHint("Team Name");
                openKeyboard(myInputName);
                break;
            case 1:myButton.setText("submit entry1 details");
                visibilitySet(true, false, false, false, true);
                myInputName.setHint("Your Name");
                openKeyboard(myInputEntry);
                break;
            case 2:myButton.setText("submit entry2 details");
                visibilitySet(true, true, false, false, true);
                myInputName.setHint("Your Name");
                openKeyboard(myInputEntry);
                break;
            case 3:myButton.setText("submit entry3 details");
                visibilitySet(true, true, true, false, true);
                myInputName.setHint("Your Name");
                openKeyboard(myInputEntry);
                break;
            default:visibilitySet(true, true, true, true, false);
                myButton.setText("confirm submission");
        }
        if (final_state<4){
            myInputName.setVisibility(View.VISIBLE);
        }else{
            myInputName.setVisibility(View.INVISIBLE);
        }
        if (final_state!=0){
            myInputEntry.setFocusable(true);
        }
    }

    public void clear(){
        myInputEntry.setText("");
        myInputName.setText("");
    }
}
