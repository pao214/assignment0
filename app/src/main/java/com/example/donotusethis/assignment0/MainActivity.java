package com.example.donotusethis.assignment0;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Elements elements;
    State state;
    MyDatafile myDatafile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //get all the elements of layout through instantiation of this class
        elements=new Elements(this);
        state=new State();
        myDatafile=new MyDatafile(this);
    }



    public void myButtonClicked(View view){
        String inputNumberInfo=elements.myInputEntry.getText().toString();
        inputNumberInfo=inputNumberInfo.toLowerCase();
        inputNumberInfo=trimWhiteSpaces(inputNumberInfo);
        String inputNameInfo=elements.myInputName.getText().toString();
        int current_state=state.getState();
        switch(current_state){
            case 0:updateTeamName(inputNameInfo);
                break;
            case 1:addTeamMember(inputNumberInfo, inputNameInfo);
                break;
            case 2:addTeamMember(inputNumberInfo, inputNameInfo);
                break;
            case 3:addTeamMember(inputNumberInfo, inputNameInfo);
                break;
            default://send data to server
        }
        elements.clear();
        elements.setStateText(state.getState());
    }

    public void editTeamNameClicked(View view){
        
    }

    public void updateTeamName(String teamName){//sets teamName both in object and view
        if(state.setTeamName(teamName)){
            elements.setMessage(null, "Please enter a non-empty name.");
        }else{
            state.updateState(1);
            elements.setEntry(0, null, teamName);
        }
    }

    public void addTeamMember(String entry_number, String person_name){
        String message=myDatafile.checkEntryNumber(entry_number);
//        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
//        message=null;
        if (message!=null){
            elements.setMessage(null, message);//message take care
        }else if (state.getState()<3){
            if(person_name.equals("")){
                elements.setMessage(null, "Please Enter a non-empty name");
            }else{
                state.add(entry_number, person_name);
                elements.setEntry(state.getState()-1, null, entry_number);
            }
        }else{
            state.add(entry_number, person_name);
            elements.setEntry(state.getState()-1, null, entry_number);
        }
    }

    public String trimWhiteSpaces(String sample){
        sample=sample.replace(" ", "");
        sample=sample.replace("   ", "");
        return sample;
    }

    public void teamNameClicked(View view){
        
    }
}
