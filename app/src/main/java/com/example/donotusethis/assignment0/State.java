package com.example.donotusethis.assignment0;

/**
 * Created by DO NOT USE THIS! on 1/9/2016.
 */
public class State {//Defines the state of the activity

    private int state;
    String teamName;
    //Represents each entry
    class Entry {
        private String person_name, entry_number;

        public Entry(String entry_number, String person_name) {
            this.entry_number = entry_number;
            this.person_name = person_name;
        }
    }
    //Collection of entries
    Entry[] entries= new Entry[3];

    public State() {//initializes state to 0
        state=0;
    }

    public int getState() {
        return state;
    }

    public void updateState(int state) {
        if (this.state<state){
            this.state=state;
        }
    }

    public void modify(int sno, String entry_number, String person_name){
        entries[sno]=new Entry(entry_number, person_name);
    }

    public void add(String entry_number, String person_name){
        entries[state-1]=new Entry(entry_number, person_name);
        state++;
    }

    public boolean setTeamName(String teamName) {
        this.teamName = teamName;
        return teamName.equals("");
    }
}
