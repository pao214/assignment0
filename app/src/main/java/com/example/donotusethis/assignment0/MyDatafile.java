package com.example.donotusethis.assignment0;

import android.app.Activity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Calendar;

/**
 * Created by DO NOT USE THIS! on 1/10/2016.
 */
public class MyDatafile {

    public MyDatafile(Activity activity) {
        //load data from a file
        InputStream inputStream=activity.getResources().openRawResource(R.raw.entries1);
        BufferedReader reader=new BufferedReader(new InputStreamReader(inputStream));
        String line=null;
        try {
            while ((line=reader.readLine())!=null){
                //code
            }
        }catch(IOException e){
            //handle exception
        }
    }

    public String checkEntryNumber(String entry_number){
        int length= entry_number.length();
        if (length!=11&&length!=12){
            return "Please check the length of the entry number typed.";
        }
        int year=0;
        try {
            year= Integer.parseInt(entry_number.substring(0, 4));
        }catch (NumberFormatException e){
            return "First four characters must be numbers.\nEg:2014cs10214";
        }
        if (year<1961){
            return "Year of admission must be atleast 1961.";
        }
        if (year>Calendar.getInstance().get(Calendar.YEAR)){
            return "Year of admission must not be in the future";
        }
        try {
            Integer.parseInt(entry_number.substring(length-5, length));
        }catch (NumberFormatException e){
            return "Last five characters must be numbers.\nEg:2014cs10214";
        }
        return null;
    }
}
